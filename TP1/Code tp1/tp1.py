# Precionando “g” guardar la porción de la imagen seleccionada como una nueva imagen,
# Precionando “r” restaurar la imagen original y permitir realizar una nueva selección,
# Precionando “q” finaliza.


import cv2
import numpy as np

blue = (255, 0, 0);
green = (0, 255, 0);
red = (0, 0, 255)
drawing = False  # true si el botón está presionado
mode = True  # si True, rectángulo, sino línea, cambia con ’m’
xybutton_down = -1, -1
xybutton_up = -1, -1  


def recorta(event, x, y, flags, param):
    global xybutton_down, xybutton_up, drawing, mode
    if event == cv2.EVENT_LBUTTONDOWN:
        drawing = True
        xybutton_down = x, y
    elif event == cv2.EVENT_MOUSEMOVE:
        if drawing is True:
            img[:] = img2
            if mode is True:
                cv2.rectangle(img, xybutton_down, (x, y), blue, 2)
    elif event == cv2.EVENT_LBUTTONUP:
        drawing = False
        xybutton_up = x, y


img = cv2.imread('gato.jpeg', 1)
img2 = cv2.imread('gato.jpeg', 1)

cv2.namedWindow(' image ')
cv2.setMouseCallback(' image ', recorta)

while (1):
    cv2.imshow(' image ', img)
    k = cv2.waitKey(1) & 0xFF
    if k == 113:  # q =113,
        break
    elif k == 114:  # r en ascii es 114
        img[:] = img2
    elif k == 103:  # g = 103
        x1, y1 = xybutton_down
        x2, y2 = xybutton_up
        img_nueva = img[y1:y2, x1:x2]
        cv2.imwrite('Imagennueva.jpg', img_nueva)
        cv2.imshow('Imagennueva.jpg', img_nueva)
cv2.destroyAllWindows()

