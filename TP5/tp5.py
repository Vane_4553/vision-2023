#! /usr/bin/env python
# -*- coding: utf-8 -*-
import cv2
import numpy as np
from object_detector import *
import numpy as np

    # Cargar aruco detector

parameters = cv2.aruco.DetectorParameters()
aruco_dict = cv2.aruco.getPredefinedDictionary(cv2.aruco.DICT_5X5_50)



    # Cargar detector de objeto
detector = HomogeneousBgDetector()

    #Configuracion para usar camara de la pc
cap = cv2.VideoCapture(0)
cap.set(cv2.CAP_PROP_FRAME_WIDTH,1280)
cap.set(cv2.CAP_PROP_FRAME_HEIGHT,720)
while True:
    _, img = cap.read()
    corners, _, _ = cv2.aruco.detectMarkers(img, aruco_dict, parameters=parameters)

    if corners:
            # obtener el poligono alrededor del aruco
        int_corners = np.int0(corners)
        cv2.polylines(img,int_corners, True, (0, 255, 0), 5)

            #Parametro del aruco
        aruco_parameter = cv2.arcLength(corners[0], True)

            #Relacion pixel - CM

        pixel_cm_ratio = aruco_parameter / 20
        counters = detector.detect_objects(img)

        for cnt in counters:
            # rectangulo al rededor de los objetos
            rect = cv2.minAreaRect(cnt)
            (x, y), (w, h), angle = rect

            # Obtener la altura y ancho de los objetos aplicando la conversion

            object_widht = w / pixel_cm_ratio
            object_height = h / pixel_cm_ratio

            # MOSTRAR RECT
            box = cv2.boxPoints(rect)
            box = np.int0(box)

            cv2.circle(img, (int(x), int(y)), 5, (0, 0, 255), -1)
            cv2.polylines(img, [box], True, (255, 0, 0), 2)
            cv2.putText(img, "width{} cm".format(round(object_widht, 1)), (int(x - 50), int(y - 15)), cv2.FONT_HERSHEY_PLAIN, 2, (100, 200, 0), 2)
            cv2.putText(img, "Height{} cm ".format(round(object_height, 1)), (int(x - 50), int(y + 15)), cv2.FONT_HERSHEY_PLAIN, 2, (100, 200, 0), 2)
    cv2.imshow("image", img)
    Key = cv2.waitKey(1)
    if Key == 27
        break
cap.release()
cv2.destroyAllWindows()
